# GOLEM vacuum pumping system RaspberryPi controller

The library of functions for reading ADC data and controlling relays are in the `vacuum.lib` module.

The tasks to be executed by the orchestrator are in the module `vacuum.tasks`.
