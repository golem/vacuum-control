import time
from .psw_gw_instek import GWInstekPSW
from .tpg262_gnome232 import TPG262GNOME232


def write2file(fname, value):
    with open(fname, 'w') as f:
        f.write(str(value))


def fill_chamber(target_pressure, eps=0.1, start_v=10,
                 interval=0.5, proportional_const=1e-1, derivative_const=1e-1,
                 max_time=60, fail_pressure=200,
                 psw_address='192.168.2.75', gauge_address='192.168.2.246',
                 verbose=False):
    psw = GWInstekPSW(psw_address)
    gauge = TPG262GNOME232(gauge_address)
    v = start_v
    psw.set_voltage(v)
    psw.set_output(True)
    last_p = p = 0
    start_t = time.time()
    while True:
        last_p = p
        p = gauge.get_pressure(1) * 1e3  # to mPa
        write2file('/dev/shm/pressure_chamber_mPa', p)
        if p > fail_pressure:
            psv.set_output(False)
            raise RuntimeError('pressure {} over limit {}'.format(p, fail_pressure))
        elapsed = time.time() - start_t
        if elapsed > max_time:
            raise RuntimeError('did not converge, maintaining pressure {}'.format(p))
        err = target_pressure - p
        change = p - last_p
        if abs(err) < eps and abs(change)/interval < eps:
            if verbose:
                print('reached pressure {} with error {} and change {} in {} seconds '.format(p, err, change, elapsed))
            break
        v += proportional_const * err - derivative_const * change / interval
        psw.set_voltage(v)
        if verbose:
            print('pressure {}, setting voltage {}'.format(p, v))
        time.sleep(interval)


def close_valve(psw_address='192.168.2.75'):
    psw = GWInstekPSW(psw_address)
    psw.set_output(False)

